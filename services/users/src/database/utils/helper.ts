const makeId = (length: number): number => {
    return Math.floor(Math.random() * Math.pow(10, length));
};

export const generateFakePhone = (): string => {
    const n = Math.floor(100000000 + Math.random() * 900000000);
    return `+99${n}`;
};

export const generateFakeEmail = (): string => {
    const e = makeId(10);
    return `${e}@delete.com`
}
