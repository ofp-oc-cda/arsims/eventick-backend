export interface UserInterface {
    id: number,
    uuid: string,
    firstname: string,
    lastname: string,
    phone: string,
    email: string | null,
    password: string | null,
    stripe_customer: string | null,
    created_at: Date,
    updated_at: Date
}
