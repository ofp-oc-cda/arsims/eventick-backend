import {QueryTypes} from "sequelize";
import {getSequelize} from "../database";

export const GetUsers = async (query) => {
    const response = await getSequelize().query(`
        select u.*
        from users u
    `, {
        raw: true,
        plain: true,
        type: QueryTypes.SELECT
    })
}
