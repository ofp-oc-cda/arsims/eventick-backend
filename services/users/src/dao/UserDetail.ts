import {getSequelize} from "../database";
import {QueryTypes} from "sequelize";

export const GetUserDetails = async (userId: number) => {
    return await getSequelize().query(`
        SELECT u.* from users u WHERE u.id = :users
    `, {
        replacements: {
            userId: userId
        },
        raw: true,
        plain: true,
        type: QueryTypes.SELECT
    })
}
