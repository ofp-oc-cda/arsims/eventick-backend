import {GetUserDetails} from "./UserDetail";
import {GetUsers} from "./GetUsers";

export {GetUserDetails, GetUsers}
