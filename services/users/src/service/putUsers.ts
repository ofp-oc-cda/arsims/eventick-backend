import {getUserDetail} from "./getUserDetail";
import {Users} from "../database/models";
import {PostUserInput} from "../lib/users";

export const putUsers = async (body: PostUserInput) => {
    try {
        if (
            !body.firstname ||
            !body.lastname ||
            !body.email ||
            !body.password ||
            !body.phone ||
            !body.stripe_customer
        ) {
            return {
                statusCode: 409,
                result: "Missing parameters"
            };
        }
    } catch (e) {

    }
}
