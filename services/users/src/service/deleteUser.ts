import {Users} from '../database/models';

export const deleteUser = async (userId: number) => {
    try {
        await Users.destroy({
            where: { id: userId }
        });
    } catch (error) {
        console.log(`[ERROR] userId: ${userId}`);
        throw error;
    }
};
