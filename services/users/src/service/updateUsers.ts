import {PostUserInput} from "../lib/users";
import {getSequelize, Users} from "../database";
import {getUserDetail} from "./getUserDetail";

export const updateUser = async (data: PostUserInput) => {
    const transaction = await getSequelize().transaction();
    try {
        if (
            !data.firstname ||
            !data.lastname ||
            !data.email ||
            !data.password ||
            !data.phone ||
            !data.stripe_customer
        ) {
            return {
                statusCode: 409,
                result: "Missing parameters"
            };
        }

        await Users.update(data, {
            where: {
                id: data.id
            },
            transaction: transaction
        });

        await transaction.commit();
        return getUserDetail(data.id);
    } catch (error) {
        console.log("[ERROR] updateUser : ", error); // log the error message
        await transaction.rollback();
        throw error;
    }
};
