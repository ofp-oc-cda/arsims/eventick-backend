import {GetUsers} from "../dao";

export const getUsers = async (query) => {
    try{
        const result = await GetUsers(query);
        return {
            statusCode: 200,
            result: result
        }
    } catch (error) {
        console.log("[ERROR] query : ", query);
        throw error;
    }
}
