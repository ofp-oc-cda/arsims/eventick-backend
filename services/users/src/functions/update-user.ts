import {updateUser} from "../service/updateUsers";

export const handler = (async request => {
    const body = request.body;

    if(!body) {
        return {
            statusCode: 400,
            result: "Missing parameters"
        }
    }

    return await updateUser(body);
})
