import {APIGatewayEvent, Context} from "aws-lambda";
import {loadSequelize} from "../database";

export const handler = async (event: APIGatewayEvent, context: Context) => {
    context.callbackWaitsForEmptyEventLoop = false;
    try {
        await loadSequelize();

        return {
            statusCode: 200,
            headers: {
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Credentials": true,
                "Content-Type": "application/json"
            },
            body: JSON.stringify('ok')
        };
    } catch (error) {
        console.log("[ERROR]", error);
        // Ensure you are closing the connexion
        return {
            statusCode: 403,
            result: error
        };
    }
}
