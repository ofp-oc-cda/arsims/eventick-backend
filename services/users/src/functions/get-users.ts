import {getUsers} from "../service/getUsers";

export const handler = (async request => {
    const query = request.selectQuery ?? null;

    return await getUsers(query);
})
