import {getUserDetail} from "../service/getUserDetail";

export const handler = (async request => {
    const userId = Number(request.pathParams?.userId);

    if(Number.isNaN(userId)) {
        return {
            statusCode: 400,
            result: "Missing parameters"
        }
    }

    return await getUserDetail(userId);
})
