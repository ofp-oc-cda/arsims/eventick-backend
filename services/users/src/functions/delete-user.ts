import {deleteUser} from "../service/deleteUser";

const handler = (async request => {
    const userId = request.userId;
    //check if userId is an integer number
    if(!Number.isInteger(userId)) {
        return {
            statusCode: 400,
            result: "Missing parameters"
        }
    }

    return await deleteUser(userId)
})
