import {Optional} from "../utils/modifyFields";
import {Comments} from "./Comments";

export interface Profile {
    id: number,
    firstname: string,
    lastname: string,
    profileImage: string,
    phone: string,
    dateOfBirth: string,
    gender: string,
    joined: Date,
    email: string,
    address: string,
    created_at: Date,
    updated_at: Date
}

export type ProfileCreate = Omit<Optional<Profile, "id">, "created_at" | "updated_at">
export type ProfileUpdate = Partial<Omit<Comments, "id" | "created_at" | "updated_at">> & Pick<Profile, "id">
