import {Optional} from "../utils/modifyFields"

export interface Comments {
    id: number,
    title: string;
    description: string,
    name: string,
    email: string,
    created_at: Date,
    updated_at: Date
}

export type CommentCreate = Omit<Optional<Comments, "id">, "created_at" | "updated_at">
export type CommentUpdate = Partial<Omit<Comments, "id" | "created_at" | "updated_at">> & Pick<Comments, "id">
