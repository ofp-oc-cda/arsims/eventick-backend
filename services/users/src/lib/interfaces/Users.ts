import {Optional} from "../utils/modifyFields";

export interface Users {
    id: number,
    uuid: number,
    firstname: string,
    lastname: string,
    email: string,
    password: string
    phone: string,
    stripe_customer: string,
    created_at: Date,
    updated_at: Date
}

export type UsersCreate = Omit<Optional<Users, "id">, "created_at" | "updated_at">
export type UsersUpdate = Partial<Omit<Users, "id" | "created_at" | "updated_at">> & Pick<Users, "id">
