import {Optional} from "../utils/modifyFields";

export interface Events {
    id: number,
    image: string,
    title: string,
    category: string[],
    description: string,
    price: number,
    created_at: Date,
    updated_at: Date,
}

export type EventsCreate = Omit<Optional<Events, "id">, "created_at" | "updated_at">
export type EventsUpdate =Partial<Omit<Events, "id" | "created_at" | "updated_at">> & Pick<Events, "id">
