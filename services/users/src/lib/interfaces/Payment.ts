import {Optional} from "../utils/modifyFields";

export interface Payment {
    id: number,
    uuid: string,
    card_token: string,
    user_id: number,
    brand: string | null,
    country: string | null,
    exp_month: number | null,
    exp_year: number | null,
    last_4: number | null,
    status: string | null,
    created_at: Date,
    updated_at: Date
}

export type PaymentCreate = Omit<Optional<Payment, "id">, "created_at" | "updated_at">
export type PaymentUpdate = Partial<Omit<Payment, "id" | "created_at" | "updated_at">> & Pick<Payment, "id">
