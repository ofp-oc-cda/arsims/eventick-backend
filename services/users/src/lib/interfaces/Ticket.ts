import { Optional } from "../utils/modifyFields";

export interface Ticket {
    id: number,
    image: string,
    title: string,
    description: string,
    price: number,
    created_at: string,
    updated_at: string
}

export type TicketCreate = Omit<Optional<Ticket, "id">, "created_at" | "updated_at">
export type TicketUpdate = Partial<Omit<Ticket, "id" | "created_at" | "updated_at">> & Pick<Ticket, "id">
