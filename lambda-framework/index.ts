import {
    APIGatewayEvent,
    APIGatewayProxyEvent,
    APIGatewayProxyEventQueryStringParameters,
    APIGatewayProxyHandler,
    APIGatewayProxyResult
} from "aws-lambda";

import {APIGatewayProxyEventPathParameters} from "aws-lambda/trigger/api-gateway-proxy"
